package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName.length( ) > 4 ;
	}
	
	public static boolean hasAlphaAndNumbersOnly( String loginName ) {
		if(loginName.length()<6)
			return false;
		
		for(int i = 0; i <loginName.length();i++)
		{
			if(Character.isAlphabetic(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	
	public static boolean startsWithAlphabet( String loginName ) {
		
		
		if(Character.isDigit(loginName.charAt(0)))
		{
			return false;
		}
		
		
		for(int i = 0; i <loginName.length();i++)
		{
			if(Character.isAlphabetic(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
}
