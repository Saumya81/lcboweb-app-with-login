package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	
	@Test
	public final void LoginNameHasAlphaCharNumbersOnlyRegular() {
		String loginName="sammehta123";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void LoginNameHasAlphaCharNumbersOnlyException() {
		String loginName= "";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void LoginNameHasAlphaCharNumbersOnlyBoundaryIn() {
		String loginName="saumyam";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void LoginNameHasAlphaCharNumbersOnlyBoundaryOut() {
		String loginName="mehtsaum&";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertFalse("The emai provided in not valid ",result);
	}

	
	
	
	//---------------------------------------------------------
	
	
	@Test
	public final void LoginNameStartsWithAlphabet() {
		String loginName="mehtsaum";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void LoginNameStartsWithAlphabetException() {
		String loginName= "&&&&&&";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void LoginNameStartsWithAlphabetBoundaryIn() {
		String loginName="sam1234";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void LoginNameStartsWithAlphabetBoundaryOut() {
		String loginName="81mehtsaum";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertFalse("The emai provided in not valid ",result);
	}

}
